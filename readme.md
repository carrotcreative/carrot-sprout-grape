# sprout-grape-api

a sprout template for creating grape apis

### Installation

- `npm install sprout-cli -g`
- `sprout add sprout-grape-api git@github.com:carrot/sprout-grape-api.git`
- `sprout new sprout-grape-api myproject `

### Options

- **name** (name of template)
- **description** (a short description of the template)
- **github_username** (name of github user)
